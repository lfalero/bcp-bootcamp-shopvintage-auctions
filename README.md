# Dockers

## Carga Inicial

https://labs.play-with-docker.com/

```shell
$ ssh keygen
$ ssh ip172-18-0-36-c1n7af1bqvp0009l4rgg@direct.labs.play-with-docker.com
```


$ docker pull mongo:4.4.5
$ docker images
$ docker run -d -p 27017:27017 --name mongo mongo:4.4.5

$ docker pull postgres:9.6.21
$ docker images
$ docker run -d -p 5432:5432 -e POSTGRES_USER=lfalero -e POSTGRES_PASSWORD=123456 --name postgres postgres:9.6.21

## Proyecto Eureka Server

```shell
$ git clone https://lfalero@bitbucket.org/lfalero/bcp-bootcamp-eurekaserver.git
$ cd bcp-bootcamp-eurekaserver
$ docker build . --tag lfalero/eurekaserver:1.0.0
$ docker images
$ docker run -d -p 8761:8761 --name bcp-bootcamp-eurekaserver lfalero/eurekaserver:1.0.0
```

## Proyecto Config Server

```shell
$ git clone https://lfalero@bitbucket.org/lfalero/bcp-bootcamp-configserver.git
$ cd bcp-bootcamp-configserver
$ docker build . --tag lfalero/configserver:1.0.0
$ docker images
$ docker run -d -p 8888:8888 --name bcp-bootcamp-configserver lfalero/configserver:1.0.0
$ docker run -d -e "EUREKA_CLIENT_SERVICEURL_DEFAULTZONE=http://localhost:8761/eureka" -p 8888:8888 --name bcp-bootcamp-configserver lfalero/configserver:1.0.0
```

## Proyecto Shopvintage Auctions

```shell
$ git clone https://bitbucket.org/lfalero/bcp-bootcamp-shopvintage-auctions.git
$ cd bcp-bootcamp-shopvintage-auctions
$ docker build . --tag lfalero/shopvintage-auctions:1.0.0
$ docker images
$ docker run --rm -p 9005:9005 --name bcp-bootcamp-shopvintage-auctions lfalero/shopvintage-auctions:1.0.0
$ docker run --rm -e "SPRING_CONFIG_IMPORT=configserver:http://localhost:8888" -p 9005:9005 --name bcp-bootcamp-shopvintage-auctions lfalero/shopvintage-auctions:1.0.0
```
